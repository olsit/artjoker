Function.prototype.myBind = function (context, ...theArgs) {
  if (typeof context === "string") {
    context = new String(context);
  }

  if (typeof context === "number") {
    context = new Number(context);
  }

  if (typeof context === "boolean") {
    context = new Boolean(context);
  }

  let thisFunction = this;
  return function(...callArgs) {
    let symbol = Symbol();
    context[symbol] = thisFunction;
    let result = context[symbol](...theArgs, ...callArgs);
    delete context[symbol];
    return result;
  };
};

Function.prototype.myCall = function (context, ...theArgs) {
  if (typeof context === "string") {
    context = new String(context);
  }

  if (typeof context === "number") {
    context = new Number(context);
  }

  if (typeof context === "boolean") {
    context = new Boolean(context);
  }

  let symbol = Symbol();
  context[symbol] = this;
  let result = context[symbol](...theArgs);
  delete context[symbol];
  return result;
};

Array.prototype.myMap = function (callback, thisArg) {
  if (typeof callback !== "function") {
    throw new Error(callback + " is not a function");
  }

  thisArg = thisArg || window;
  let result = [];

  for (let i = 0; i < this.length; i++) {
    result.push(callback.myCall(thisArg, this[i], i, this));
  }

  return result;
};

Array.prototype.myFilter = function (callback, thisArg) {
  if (typeof callback !== "function") {
    throw new Error(callback + " is not a function");
  }

  thisArg = thisArg || window;
  let result = [];

  for (let i = 0; i < this.length; i++) {
    if (callback.myCall(thisArg, this[i], i, this)) {
      result.push(this[i]);
    }
  }

  return result;
};

Array.prototype.myFind = function (callback, thisArg) {
  if (typeof callback !== "function") {
    throw new Error(callback + " is not a function");
  }

  thisArg = thisArg || window;

  for (let i = 0; i < this.length; i++) {
    if (callback.myCall(thisArg, this[i], i, this)) {
      return this[i];
    }
  }
};

Array.prototype.myForEach = function (callback, thisArg) {
  if (typeof callback !== "function") {
    throw new Error(callback + " is not a function");
  }

  thisArg = thisArg || window;

  for (let i = 0; i < this.length; i++) {
    callback.myCall(thisArg, this[i], i, this);
  }
};

Array.prototype.myReduce = function (callback, initialValue) {
  if (typeof callback !== "function") {
    throw new Error(callback + " is not a function");
  }

  let result = initialValue || this[0];

  for (let i = (initialValue ? 0 : 1); i < this.length; i++) {
    result = callback(result, this[i], i, this);
  }

  return result;
};
