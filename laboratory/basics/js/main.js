const checkIsAnagram = (word1, word2) => {
  if (typeof word1 !== "string" || typeof word2 !== "string") {
    throw new Error("One of the arguments is not a string");
  }

  if (word1 === "" || word2 === "") {
    throw new Error("String cannot be empty");
  }

  if (word1.length !== word2.length) {
    return false;
  }

  if (word1 === word2) {
    return true;
  }

  let wordСharacters = {};

  for (let i = 0; i < word1.length; i++) {
    wordСharacters[i] = wordСharacters[i] || 0;
    for (let j = wordСharacters[i]; j < word2.length; j++ ) {
      if (word1[i] === word2[j]) {
        wordСharacters[i] = ++j;
        break;
      }
      if (j === word2.length - 1) {
        return false;
      }
    }
  }

  return true;
};

const amountDigitsNumber = num => {
  if (typeof num !== "number" || Number.isNaN(num)) {
    throw new Error("Argument is not a number");
  }

  if (!isFinite(num)) {
    throw new Error("Number cannot be infinity");
  }

  let result = num === 0 ? 1 : 0;

  while (num !== 0) {
    num = parseInt(num / 10);
    result++;
  }

  return result;
};

const amountDigitsNumberRecursion = (num, numberLength) => {
  if (typeof num !== "number" || Number.isNaN(num)) {
    throw new Error("Argument is not a number");
  }

  if (!isFinite(num)) {
    throw new Error("Number cannot be infinity");
  }

  numberLength = numberLength || 0;

  num = parseInt(num / 10);
  numberLength++;

  if (num === 0) {
    return numberLength;
  }

 return amountDigitsNumberRecursion(num, numberLength);
};

const checkIsPalindrome = word => {
  if (typeof word !== "string") {
    throw new Error("Argument is not a string");
  }

  if (word === "") {
    throw new Error("String cannot be empty");
  }

  for (let i = 0; i < word.length; i++) {
    if (word[i] !== word[word.length - i - 1]) {
      return false;
    }
  }

  return true;
};

const quantityUniqueWords = str => {
  if (typeof str !== "string") {
    throw new Error("Argument is not a string");
  }

  if (str === "") {
    return 0;
  }

  let uniqueWords = {};
  let element = "";
  let result = 0;

  str = str.toLowerCase().trim();
  str = str.replace(/[^\dа-яА-Яa-zA-ZЁёҐґЄєІіЇї\s]/gi, "").replace(/  +/gi, " ");

  for (let i = 0; i < str.length; i++) {
    if (str[i] === " ") {
      if (uniqueWords[element] === undefined) {
        uniqueWords[element] = result++;
      }
      element = "";
    } else {
      element += str[i];
    }

    if (i === str.length - 1 && element !== "") {
      if (uniqueWords[element] === undefined) {
        uniqueWords[element] = result++;
      }
    }
  }

  return result;
};

const uniqueWords = str => {
  if (typeof str !== "string") {
    throw new Error("Argument is not a string");
  }

  let result = {};
  let element = "";

  str = str.toLowerCase().trim();
  str = str.replace(/[^\dа-яА-Яa-zA-ZЁёҐґЄєІіЇї\s]/gi, "").replace(/  +/gi, " ");

  for (let i = 0; i < str.length; i++) {
    if (str[i] === " ") {
      result[element] = ++result[element] || 1;
      element = "";
    } else {
      element += str[i];
    }

    if (i === str.length - 1 && element !== "") {
      result[element] = ++result[element] || 1;
    }
  }

  return result;
};

class Rectangle {
  constructor(width, height) {
    if (width <= 0 || height <= 0) {
      throw new Error("The sides of the rectangle cannot be less than 0");
    }

    if (typeof width !== "number"
      || Number.isNaN(width)
      || typeof height !== "number"
      || Number.isNaN(height)) {
      throw new Error("The length and width of the rectangle must be a number");
    }

    if (!isFinite(width) || !isFinite(height)) {
      throw new Error("Number cannot be infinity");
    }

    this.width = width;
    this.height = height;
  }

  getPerimeter() {
    return (this.width * 2) + (this.height * 2);
  }

  getArea() {
    return this.width * this.height;
  }
}

function RectangleConstructor(width, height) {
  if (width <= 0 || height <= 0) {
    throw new Error("The sides of the rectangle cannot be less than 0");
  }

  if (typeof width !== "number"
    || Number.isNaN(width)
    || typeof height !== "number"
    || Number.isNaN(height)) {
    throw new Error("The length and width of the rectangle must be a number");
  }

  if (!isFinite(width) || !isFinite(height)) {
    throw new Error("Number cannot be infinity");
  }

  this.width = width;
  this.height = height;
}

RectangleConstructor.prototype.getPerimeter = function () {
  return (this.width * 2) + (this.height * 2);
};

RectangleConstructor.prototype.getArea = function () {
  return this.width * this.height;
};

class Triangle {
  constructor(side1, side2, baseOfTriangl) {
    if (side1 <= 0 || side2 <= 0 || baseOfTriangl <= 0) {
      throw new Error("Sides of a triangle cannot be less than 0");
    }

    if (typeof side1 !== "number"
      || Number.isNaN(side1)
      || typeof side2 !== "number"
      || Number.isNaN(side2)
      || typeof baseOfTriangl !== "number"
      || Number.isNaN(baseOfTriangl)) {
      throw new Error("The passed length parameter, one of the sides, is not a number");
    }

    if (!isFinite(side1) || !isFinite(side2) || !isFinite(baseOfTriangl)) {
      throw new Error("Number cannot be infinity");
    }

    this.side1 = side1;
    this.side2 = side2;
    this.baseOfTriangl = baseOfTriangl;
  }

  getPerimeter() {
    return this.side1 + this.side2 + this.baseOfTriangl;
  }

  getArea() {
    let semiPerimeter = (this.side1 + this.side2 + this.baseOfTriangl) / 2;
    return Math.sqrt(semiPerimeter * (semiPerimeter - this.side1) * (semiPerimeter - this.side2) * (semiPerimeter - this.baseOfTriangl));
  }
}

function TriangleConstructor(side1, side2, baseOfTriangl) {
  if (side1 <= 0 || side2 <= 0 || baseOfTriangl <= 0) {
    throw new Error("Sides of a triangle cannot be less than 0");
  }

  if (typeof side1 !== "number"
    || Number.isNaN(side1)
    || typeof side2 !== "number"
    || Number.isNaN(side2)
    || typeof baseOfTriangl !== "number"
    || Number.isNaN(baseOfTriangl)) {
    throw new Error("The passed length parameter, one of the sides, is not a number");
  }

  if (!isFinite(side1) || !isFinite(side2) || !isFinite(baseOfTriangl)) {
    throw new Error("Number cannot be infinity");
  }

  this.side1 = side1;
  this.side2 = side2;
  this.baseOfTriangl = baseOfTriangl;
}

TriangleConstructor.prototype.getPerimeter = function () {
  return this.side1 + this.side2 + this.baseOfTriangl;
};

TriangleConstructor.prototype.getArea = function () {
  let semiPerimeter = (this.side1 + this.side2 + this.baseOfTriangl) / 2;
  return Math.sqrt(semiPerimeter * (semiPerimeter - this.side1) * (semiPerimeter - this.side2) * (semiPerimeter - this.baseOfTriangl));
};

class Circle {
  constructor(radius) {
    if (radius <= 0) {
      throw new Error("Circle radius cannot be less than 0");
    }

    if (typeof radius !== "number" || Number.isNaN(radius)) {
      throw new Error("Radius is not a number");
    }

    if (!isFinite(radius)) {
      throw new Error("Number cannot be infinity");
    }

    this.radius = radius;
  }

  getPerimeter() {
    return 2 * Math.PI * this.radius;
  }

  getArea() {
    return Math.PI * Math.pow(this.radius, 2);
  }
}

function CircleConstructor(radius) {
  if (radius <= 0) {
    throw new Error("Circle radius cannot be less than 0");
  }

  if (typeof radius !== "number" || Number.isNaN(radius)) {
    throw new Error("Radius is not a number");
  }

  if (!isFinite(radius)) {
    throw new Error("Number cannot be infinity");
  }

  this.radius = radius;
}

CircleConstructor.prototype.getPerimeter = function () {
  return 2 * Math.PI * this.radius;
};

CircleConstructor.prototype.getArea = function () {
  return Math.PI * Math.pow(this.radius, 2);
};

const factorial = num => {
  if (typeof num !== "number" || Number.isNaN(num)) {
    throw new Error("Argument is not a number");
  }

  if (!isFinite(num)) {
    throw new Error("Number cannot be infinity");
  }

  if (num < 0) {
    throw new Error("Unable to calculate factorial of a negative value");
  }

  if (num > 1) {
    return num * factorial(--num);
  }

  return 1;
};

const factorialMemoized = function (){
  let cachFactorial = {};

  return function factorial(num) {
    if (typeof num !== "number" || Number.isNaN(num)) {
      throw new Error("Argument is not a number");
    }

    if (!isFinite(num)) {
      throw new Error("Number cannot be infinity");
    }

    if (num < 1) {
      throw new Error("Unable to calculate factorial of a negative value");
    }

    if (cachFactorial[num] !== undefined) {
      return cachFactorial[num];
    }

    if (num > 1) {
      cachFactorial[num] = num * factorial(num - 1);
      return cachFactorial[num];
    }

    return 1;
  };
}();

const sumArrayElement = (arr, callback) => {
  if (typeof callback !== "function") {
    throw new Error("Callback not a function");
  }

  if (!Array.isArray(arr)) {
    throw new Error("Argument 'arr' is not a Array");
  }

  let result = 0;

  for (let i = 0; i < arr.length; i++) {
    if (callback(arr[i])) {
      if (result === 0) {
        result = arr[i];
      }
      result += arr[i];
    }
  }

  return result;
};

const sumArrayElementRecursion = (arr, callback, result, item) => {
  if (typeof callback !== "function") {
    throw new Error("Callback not a function");
  }

  if (!Array.isArray(arr)) {
    throw new Error("Argument 'arr' is not a Array");
  }

  item = item || 0;
  result = result || 0;

  if (item >= arr.length) {
    return result;
  }

  if (callback(arr[item])) {
    if (result === 0) {
      result = arr[item++];
    }
    result += arr[item++];
    return sumArrayElementRecursion(arr, callback, result, item);
  }

  return result;
};

const quantityArrayElement = (arr, callback) => {
  if (typeof callback !== "function") {
    throw new Error("Callback not a function");
  }

  if (!Array.isArray(arr)) {
    throw new Error("Argument 'arr' is not a Array");
  }

  let result = 0;

  for (let i = 0; i < arr.length; i++) {
    if (callback(arr[i])) {
      result++;
    }
  }

  return result;
};

const decimalToBinary = num => {
  if (typeof num !== "number" || Number.isNaN(num)) {
    throw new Error("Argument is not a number");
  }

  if (!isFinite(num)) {
    throw new Error("Number cannot be infinity");
  }

  if (num < 0) {
    throw new Error("Number must not be negative");
  }

  let result = num === 0 ? "0" : "";

  while (num > 0) {
    result += (num % 2);
    num = parseInt(num / 2);
  }

  return result;
};

const binaryToDecimal = binary => {
  if (typeof binary !== "string") {
    throw new Error("Argument is not a string");
  }

  if (!isFinite(binary)) {
    throw new Error("Number cannot be infinity");
  }

  if (binary === "") {
    throw new Error("String cannot be empty");
  }

  let result = 0;
  binary = binary.replace(/\D/gi, "");

  for (let i = 0; i < binary.length; i++) {
    result += (binary[binary.length - 1 - i] * Math.pow(2, i));
  }

  return result;
};

const sumTwoDimensionalArrayElement = (arr, callback) => {
  if (typeof callback !== "function") {
    throw new Error("Callback not a function");
  }

  if (!Array.isArray(arr)) {
    throw new Error("Argument is not a Array");
  }

  let result = 0;

  for (let i = 0; i < arr.length; i++) {
    if (Array.isArray(arr[i])) {
      for (let j = 0; j < arr[i].length; j++) {
        if (callback(arr[i][j])) {
          if (result === 0) {
            result = arr[i][j];
          }
          result += arr[i][j];
        }
      }
    } else {
      throw new Error("Array is not two dimensional");
    }
  }

  return result;
};

const quantityTwoDimensionalArrayElement = (arr, callback) => {
  if (typeof callback !== "function") {
    throw new Error("Callback not a function");
  }

  if (!Array.isArray(arr)) {
    throw new Error("Argument 'arr' is not a Array");
  }

  if (arr.length === 0) {
    return 0;
  }

  let result = 0;

  for (let i = 0; i < arr.length; i++) {
    if (Array.isArray(arr[i])) {
      for (let j = 0; j < arr[i].length; j++) {
        if (callback(arr[i][j])) {
          result++;
        }
      }
    } else {
      throw new Error("Array is not two dimensional");
    }
  }

  return result;
};

const sumElementsBetweenNumber = (num1, num2, callback) => {
  if (typeof num1 !== "number"
    || typeof num2 !== "number"
    || Number.isNaN(num1)
    || Number.isNaN(num2)) {
    throw new Error("Argument is not a number");
  }

  if (!isFinite(num1) || !isFinite(num2)) {
    throw new Error("Number cannot be infinity");
  }

  if (typeof callback !== "function") {
    throw new Error("Callback not a function");
  }

  if (num1 === num2) {
    return num1;
  }

  let result = 0;
  let min = Math.min(num1, num2);
  let max = Math.max(num1, num2);

  if (min < max) {
    while (min <= max) {
      if (callback(min)) {
        result += min;
      }
      min++;
    }
  }

  return result;
};

const sumElementsBetweenNumberRecursion = (num1, num2, callback, result, item) => {
  if (typeof num1 !== "number"
    || typeof num2 !== "number"
    || Number.isNaN(num1)
    || Number.isNaN(num2)) {
    throw new Error("Argument is not a number");
  }

  if (!isFinite(num1) || !isFinite(num2)) {
    throw new Error("Number cannot be infinity");
  }

  if (typeof callback !== "function") {
    throw new Error("Callback not a function");
  }

  if (num1 === num2) {
    return num1;
  }

  let min = Math.min(num1, num2);
  let max = Math.max(num1, num2);

  item = item || min;
  result = result || 0;

  if (item !== undefined) {
    if (callback(item)) {
      result += item;
    }

    if (++item < max) {
      return sumElementsBetweenNumberRecursion(num1, num2, callback, result, item);
    }
  }

  return result;
};

const meanArrayElement = (arr, callback) => {
  if (typeof callback !== "function") {
    throw new Error("Callback not a function");
  }

  if (!Array.isArray(arr)) {
    throw new Error("Argument 'arr' is not a Array");
  }

  if (arr.length === 0) {
    throw new Error("Array must not be empty");
  }

  let counter = 0;
  let result = 0;

  for (let i = 0; i < arr.length; i++) {
    if (callback(arr[i])) {
      result += arr[i];
      counter++;
    }
  }

  if (counter === 0) {
    return result;
  }

  return result / counter;
};

const meanTwoDimensionalArrayElement = (arr, callback) => {
  if (typeof callback !== "function") {
    throw new Error("Callback not a function");
  }

  if (!Array.isArray(arr)) {
    throw new Error("Argument 'arr' is not a Array");
  }

  if (arr.length === 0) {
    throw new Error("Array must not be empty");
  }

  let callbackFilteredArray = [];
  let result = 0;

  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < arr[i].length; j++) {
      if (callback(arr[i][j])) {
        callbackFilteredArray.push(arr[i][j]);
      }
    }
  }

  for (let i = 0; i < callbackFilteredArray.length; i++) {
    result += callbackFilteredArray[i];
  }

  return result / callbackFilteredArray.length;
};

const transposedMatrix = matrix => {
  if (!Array.isArray(matrix)) {
    throw new Error("Argument 'arr' is not a Array");
  }

  if (matrix.length === 0) {
    throw new Error("Array must not be empty");
  }

  let result = {};

  for (let i = 0; i < matrix.length; i++) {
    if (matrix.length === matrix[i].length) {
      for (let j = 0; j < matrix[i].length; j++) {
        if (result[j] === undefined) {
          result[j] = [matrix[i][j]];
        } else {
          result[j].push(matrix[i][j]);
        }
      }
    } else {
      throw new Error("Array is not a matrix");
    }
  }

  return Object.values(result);
};

const sumMatrix = (matrix1, matrix2) => {
  if (!Array.isArray(matrix1) || !Array.isArray(matrix2)) {
    throw new Error("Argument is not a Array");
  }

  if (matrix1.length === 0 || matrix2.length === 0) {
    throw new Error("Matrix must not be empty");
  }

  let result = [];

  for (let i = 0; i < matrix1.length; i++) {
    result.push([]);
    if (matrix2[i].length === matrix1[i].length) {
      for (let j = 0; j < matrix1[i].length; j++) {
        result[i].push(matrix1[i][j] + matrix2[i][j]);
      }
    } else {
      throw new Error("Array is not a matrix");
    }
  }

  return result;
};

const removeArrayRow = (arr, element) => {
  if (!Array.isArray(arr)) {
    throw new Error("Argument 'arr' is not a Array");
  }

  if (arr.length === 0) {
    throw new Error("Array must not be empty");
  }

  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < arr[i].length; j++) {
      if (arr[i][j] === element) {
        arr.splice(i, 1);
        break;
      }
    }
  }
};

const removeArrayCol = (arr, element) => {
  if (!Array.isArray(arr)) {
    throw new Error("Argument 'arr' is not a Array");
  }

  if (arr.length === 0) {
    throw new Error("Array must not be empty");
  }

  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < arr[i].length; j++) {
      if (arr[i][j] === element) {
        for (let k = 0; k < arr.length; k++) {
          arr[k].splice(j, 1);
        }
        j--;
      }
    }
  }
};

const elementsMatrixOnDiagonal = (matrix, callback) => {
  if (typeof callback !== "function") {
    throw new Error("Callback not a function");
  }

  if (!Array.isArray(matrix)) {
    throw new Error("Argument 'matrix' is not a Array");
  }

  if (matrix.length === 0) {
    throw new Error("Array must not be empty");
  }

  let result = [];

  for (let i = 0; i < matrix.length; i++) {
    if (matrix.length === matrix[i].length) {
      for (let j = 0; j < matrix[i].length; j++) {
        if (i === j) {
          result.push(matrix[i][j]);
        }
      }
    } else {
      throw new Error("Array is not a matrix");
    }
  }

  return callback(result);
};

const elementsMatrixUnderDiagonal = (matrix, callback) => {
  if (typeof callback !== "function") {
    throw new Error("Callback not a function");
  }

  if (!Array.isArray(matrix)) {
    throw new Error("Argument 'matrix' is not a Array");
  }

  if (matrix.length === 0) {
    throw new Error("Array must not be empty");
  }

  let result = [];

  for (let i = 0; i < matrix.length; i++) {
    if (matrix.length === matrix[i].length) {
      for (let j = 0; j < matrix[i].length; j++) {
        if (i > j) {
          result.push(matrix[i][j]);
        }
      }
    } else {
      throw new Error("Array is not a matrix");
    }
  }

  return callback(result);
};

const elementsMatrixAboveDiagonal = (matrix, callback) => {
  if (typeof callback !== "function") {
    throw new Error("Callback not a function");
  }

  if (!Array.isArray(matrix)) {
    throw new Error("Argument 'matrix' is not a Array");
  }

  if (matrix.length === 0) {
    throw new Error("Array must not be empty");
  }

  let result = [];

  for (let i = 0; i < matrix.length; i++) {
    if (matrix.length === matrix[i].length) {
      for (let j = 0; j < matrix[i].length; j++) {
        if (i < j) {
          result.push(matrix[i][j]);
        }
      }
    } else {
      throw new Error("Array is not a matrix");
    }
  }

  return callback(result);
};

const fibonacciMemoized = function (){
  let cachFibonacci = {
    1: 0,
    2: 1
  };

  return function fibonacci(num) {
    if (typeof num !== "number") {
      throw new Error("Argument is not a number");
    }

    if (!isFinite(num)) {
      throw new Error("Number cannot be infinity");
    }

    if (num < 1) {
      throw new Error("Unable to calculate fibonacci of a negative value");
    }

    if (cachFibonacci[num] !== undefined) {
      return cachFibonacci[num];
    }

    if (num >= 1) {
      if (cachFibonacci[num] === undefined) {
        let result = fibonacci(num - 1) + fibonacci(num - 2);
        cachFibonacci[num] = result;
      }
      return cachFibonacci[num];
    }
  };
}();

function* fibonacciGen (num){
  if (typeof num !== "number") {
    throw new Error("Argument is not a number");
  }

  if (!isFinite(num)) {
    throw new Error("Number cannot be infinity");
  }

  let firstElement = 0;
  let secondElement = 1;
  let fibonacci = 0;

  for (let i = 2; i <= num; i++) {
    fibonacci = firstElement + secondElement;
    firstElement = secondElement;
    secondElement = fibonacci;
    yield fibonacci;
  }
}

let fibanacci = {
  num: 10,
  [Symbol.iterator]() {
    let i = 2;
    return {
      num: this.num,
      0: 0,
      1: 1,
      next() {
        if (i <= this.num) {
          this[i] = this[i - 1] + this[i - 2];
          return {value: this[i++], done: false};
        }
        return {value: undefined, done: true};
      }
    };
  }
};

const trafficLights = function* (){
  while (true) {
    yield "red";
    yield "yellow";
    yield "green";
    yield "yellow";
  }
}();

let trafficLightsIterator = {
  counter: 10,
  [Symbol.iterator]() {
    return {
      counter: this.counter,
      i: 0,
      0: "red",
      1: "yellow",
      2: "green",
      3: "yellow",
      next() {
        this.counter--;
        if (this.counter >= 0) {
          if (this.i === 4) {
            this.i = 0;
          }
          return {value: this[this.i++], done: false};
        }
        return {value: undefined, done: true};
      }
    };
  }
};

const checkIsPositive = num => {
  if (typeof num !== "number") {
    throw new Error("Argument is not a number");
  }

  return num >> -1 === 0;
};

const getCharacters = num => {
  if (typeof num !== "number" ) {
    throw new Error("Argument is not a number");
  }

  let result = {
    0: 0,
    1: 0
  };

  for (let i = 0; i < 32; i++) {
    if ((num & (1 << i)) === 0) {
      ++result[0];
    } else {
      ++result[1];
    }
  }

  return result;
};

const tilda1 = num => {
  if (typeof num !== "number") {
    throw new Error("Argument is not a number");
  }

  return (num * -1) - 1;
};

const tilda2 = num => {
  if (typeof num !== "number") {
    throw new Error("Argument is not a number");
  }

  return num ^ -1;
};

const tilda3 = num => {
  if (typeof num !== "number") {
    throw new Error("Argument is not a number");
  }

  let result = 0;

  for (let i = 0; i < 32; i++) {
    if ((num & (1 << i)) === 0) {
      result = result | (1 << i);
    }
  }

  return result;
};

// task2 and task13 https://miro.com/app/board/uXjVOOkY-V0=/
