// Что такое композиция генераторов?
// Грубо говоря генератор в генераторе. Если я хочу вывести в текущей итерации результат вызова другого генератора.


// Что такое итератор, написать пример итератора?/
// Объект в котором есть метод next() и этот обьект может обращатся к нему. И результат буеть с учетом выполнения прошлого раза.

  let fibanacci = {
  num: 10,
  [Symbol.iterator]() {
    let i = 2;
    return {
      num: this.num,
      0: 0,
      1: 1,
      next() {
        if (i <= this.num) {
          this[i] = this[i - 1] + this[i - 2];
          return {value: this[i++], done: false};
        }
        return {value: undefined, done: true};
      }
    };
  }
};

const trafficLights = function* (){
  while (true) {
    yield "red";
    yield "yellow";
    yield "green";
    yield "yellow";
  }
}();
